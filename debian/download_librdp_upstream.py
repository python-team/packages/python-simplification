#!/usr/bin/python3

"""
Download librdp.so from upstream package
"""

import io
import os
import zipfile
import logging
import tarfile
import argparse
from typing import List

import requests


USER_AGENT = "Mozilla/5.0 (iPhone; CPU iPhone OS 12_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) FxiOS/40.0 Mobile/15E148 Safari/605.1.15"

# Seems there is no other proper way to bypass debian proxy being set while building...
SESSION = requests.Session()
SESSION.trust_env = False


def cli_args() -> argparse.Namespace:
    """
    Command line arguments
    """

    parser = argparse.ArgumentParser(description=__doc__.strip(), formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        "--version",
        type=str,
        help="Upstream version to download",
        metavar="0.5.21",
        required=True,
    )
    parser.add_argument(
        "--architectures",
        type=str,
        nargs="+",
        help="Debian architectures to match, multiple values can be passed to help matching",
        metavar=("amd64", "x86_64"),
        required=True,
    )
    parser.add_argument(
        "--python-version",
        type=str,
        help="Python version to use",
        metavar="3.9",
        required=True,
    )
    parser.add_argument(
        "--destination",
        type=str,
        help="Destination file for librdp.so",
        metavar="/tmp/librdp.so",
        required=True,
    )
    return parser.parse_args()


def get_matching_asset_url(version: str, architectures: List[str], python_version: str, destination: str) -> str:
    """
    Use GitHub API to file upstream asset URL corresponding to given version and architecture
    """

    # List releases and find corresponding one
    releases_url = "https://api.github.com/repos/urschrei/simplification/releases"
    resp_releases = SESSION.get(releases_url, headers={"User-Agent": USER_AGENT})
    resp_releases.raise_for_status()
    releases = resp_releases.json()
    for release in releases:
        if release["tag_name"] in [version, "v%s" % version]:
            assets_url = release["assets_url"]
            logging.info("Found matching release %s", release["tag_name"])
            break
    else:
        raise ValueError("Unable to find any release at %s matching version %s" % (releases_url, version))

    # List assets for selected release and find corresponding one
    resp_assets = SESSION.get(assets_url, headers={"User-Agent": USER_AGENT})
    resp_assets.raise_for_status()
    assets = resp_assets.json()

    architectures_lower = [x.lower() for x in architectures]
    python_version_split = python_version.split(".")
    for asset in assets:
        asset_name_lower = asset["name"].lower()
        if any(
            "-cp%s%s-" % (python_version_split[0], python_version_split[1]) in asset_name_lower and "linux" in asset_name_lower and x in asset_name_lower
            for x in architectures_lower
        ):
            asset_url = asset["browser_download_url"]
            logging.info("Found matching asset %s", asset["name"])
            break
    else:
        raise ValueError("Unable to find any assets at %s matching linux with architecture %s" % (assets_url, ",".join(architectures)))

    # Download asset and extract librdp.so
    resp_asset = SESSION.get(asset_url, headers={"User-Agent": USER_AGENT})
    resp_asset.raise_for_status()
    asset_content = resp_asset.content

    # Extract librdp.so from WHL file (zip)
    zip_file = io.BytesIO(asset_content)

    zip_ = zipfile.ZipFile(zip_file, "r")
    zip_members = zip_.infolist()
    for zip_member in zip_members:
        if os.path.basename(zip_member.filename) == "librdp.so":
            librdp_content = zip_.read(zip_member)
            logging.info("File librdp.so extracted from wheel file")
            break
    else:
        raise ValueError("No librdp.so file found in wheel: %s" % ",".join(x.filename for x in zip_members))

    # Save librdp.so to file
    with open(destination, "wb") as out_fh:
        out_fh.write(librdp_content)
        logging.info("Created file %s", destination)


if __name__ == "__main__":

    logging.basicConfig(level=logging.INFO, format="%(levelname)-8s [DownloadLibRdpSo] %(message)s")
    CONFIG = cli_args()
    get_matching_asset_url(CONFIG.version, CONFIG.architectures, CONFIG.python_version, CONFIG.destination)
